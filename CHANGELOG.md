<!--
  Copyright (C) 2020,2021,2022,2024  CismonX <admin@cismon.net>
  
  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->

# Changelog

## v0.3.0 - 04/04/2024

* Add configuration for customization variables (`makeinfo -c KEY=VAL`).
* Support GNU Texinfo 7.1.

## v0.2.4 - 02/02/2022

* Fix a bug which breaks the snippet completion of block commands.

## v0.2.3 - 10/23/2021

* Fix a bug which prevents diagnostic info from displaying correctly in GNU Texinfo 6.8, and on Windows platform.

## v0.2.2 - 10/03/2021

* Fix a bug which breaks configuration `preview.includePaths` on Windows.
* Remove completion for commands which are deprecated in GNU Texinfo 6.8.

## v0.2.1 - 05/05/2021

* Fix an error in build script which produces corrupted metadata in `.vsix` package.

## v0.2.0 - 05/05/2021

* Add version indicator (as status bar item).
* Add user manual.
* Optimize code completion.
* Optimize configuration items.
* Optimize HTML preview.
* Change license from MIT to GPL-3.0-or-later.

## v0.1.4 - 02/23/2021

* Support loading custom CSS for HTML preview.

## v0.1.3 - 02/09/2021

* Fix a bug in completion of block command `@detailmenu`.

## v0.1.2 - 12/31/2020

* Update the "go-to-file" icon ID in Code Lens, as the original icon ID no longer exists in the latest version of VSCode.

## v0.1.1 - 11/30/2020

* Fix a bug in completion of brace command `@code`.

## v0.1.0 - 11/16/2020

* Initial release.
