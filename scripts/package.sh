#!/bin/sh
#
# Copyright (C) 2021,2024  CismonX <admin@cismon.net>
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty, provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
#

set -e

version=$(json -f package.json version)
vsix_file_name=texinfo-$version.vsix
tmpdir="$vsix_file_name.d"

vsce package --baseContentUrl="$(json -f package.json repository.url)/tree"
test -e "$tmpdir" && rm -r "$tmpdir"
unzip -d "$vsix_file_name.d" "$vsix_file_name"

cd "$tmpdir"
minify_xml() {
    minify-xml --no-shorten-namespaces --no-remove-unused-namespaces  \
        --no-remove-unused-default-namespace -i "$1"
}
minify_xml '[Content_Types].xml'
minify_xml extension.vsixmanifest

cd extension
# Minify JSON files.
json -j0 -I -e "$(cat ../../scripts/package-json-cleanup.js)" -f package.json
perl -pi -e 'chomp if eof' package.json
json5 language-configuration.json > _ && mv _ language-configuration.json
# Remove comments from Markdown files.
tail -n +9 readme.md > _ && mv _ readme.md
tail -n +9 changelog.md > _ && mv _ changelog.md

cd ext
# Minify Perl scripts.
# You can install perltidy with `cpan Perl::Tidy`
if command -v perltidy; then
    < html-preview.pm perltidy --mangle -dac  \
        | tr -d '\n' > _ && mv _ html-preview.pm
fi

cd ../../..
# Re-package .vsix file.
node ./scripts/make-vsix.js "$vsix_file_name" "$(git log -1 --format=%cd)"
rm -r "$tmpdir"
