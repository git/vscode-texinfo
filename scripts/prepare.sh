#!/bin/sh
#
# Copyright (C) 2020,2021,2024  CismonX <admin@cismon.net>
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty, provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.
#

set -e

src_path=./node_modules/language-texinfo
dest_path=./out/grammars

mkdir -p $dest_path

# Convert TextMate grammar to JSON, since VSCode cannot recognize CSON ones.
cson2json $src_path/grammars/texinfo.cson | json5 > $dest_path/texinfo.json

version=$(json -f package.json version)
echo "@set VERSION $version" > ./doc/version.texi
