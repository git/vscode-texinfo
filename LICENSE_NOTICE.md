<!--
  Copyright (C) 2021  CismonX <admin@cismon.net>

  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->

# License Notice

## Other project files

Project files listed below cannot carry a license notice by themselves, due to
file format restrictions.

    assets/texinfo.png

This file is released into the public domain using [CC0].

## Files from other projects

Source code from the projects listed below are *not* part of vscode-texinfo.
However, when building the project, they are downloaded, compiled, and packaged
into a single binary file alongside with vscode-texinfo.

| Project                       | Copyright Holder | License |
| -                             | -                | -       |
| [Texinfo syntax highlighting] | John Gardner     | [ISC](https://github.com/Alhadis/language-texinfo/blob/master/LICENSE.md) |

The following projects are required during runtime of vscode-texinfo, but as
separate programs.

| Project              | Copyright Holder         | License |
| -                    | -                        | -       |
| [Visual Studio Code] | Microsoft Corporation    | [MIT](https://github.com/microsoft/vscode/blob/main/LICENSE.txt) |
| [GNU Texinfo]        | Free Software Foundation | [GPL-3.0](https://git.savannah.gnu.org/cgit/texinfo.git/tree/COPYING)-or-later |


<!-- Reference Links -->

[CC0]:                         https://creativecommons.org/public-domain/cc0/
[Texinfo syntax highlighting]: https://github.com/Alhadis/language-texinfo
[Visual Studio Code]:          https://github.com/microsoft/vscode
[GNU Texinfo]:                 https://www.gnu.org/software/texinfo
