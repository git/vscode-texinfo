<!--
  Copyright (C) 2020,2021,2024  CismonX <admin@cismon.net>
  
  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->

# vscode-texinfo

[![License]](LICENSE)

## About

vscode-texinfo is an extension of [Visual Studio Code] which provides the
following features for editing [Texinfo] documents:

* Syntax Highlighting
* Code Completion
* HTML Preview
* Block Folding
* Breadcrumb Navigation
* Diagnostics

## Getting Started

For instructions on how to install, use, and contribute to vscode-texinfo,
see the [user manual].

<!-- Reference Links -->

[Visual Studio Code]: https://github.com/microsoft/vscode
[Texinfo]: https://www.gnu.org/software/texinfo/
[License]: https://img.shields.io/badge/license-GPL--3.0--or--later-blue.svg
[user manual]: https://nongnu.org/vscode-texinfo/manual
